--------------------------------------------------------
-- Minetest :: Holoblock Mod v1.0 (holoblock)
--
-- See README.txt for licensing and other information.
-- Copyright (c) 2017-2019, Leslie E. Krause
--------------------------------------------------------

-- name, nodes, max_light, min_light, chance, active_object_count, max_height, min_height, spawn_func

local holo_defs = { }
local holo_objs = { }

holo_defs[ 1 ] = { name = "oerrki", x = -4, y = 2, z = 0, yaw = -90, size = { x = 40, y = 40 }, anim_start = 0, anim_end = 23, anim_speed = 15, mesh = "mobs_oerkki.x", textures = { "mobs_oerkki.png" } }
holo_defs[ 2 ] = { name = "stoney", x = -4, y = 2, z = 0, yaw = -90, size = { x = 24, y = 20 }, anim_start = 40, anim_end = 63, anim_speed = 15, mesh = "mobs_stone_monster.x", textures = { "mobs_stone_monster.png" } }
holo_defs[ 3 ] = { name = "maike", x = -4, y = 10, z = 0, yaw = -90, size = { x = 8, y = 8 }, anim_start = 0, anim_end = 79, anim_speed = 30, mesh =
			"3d_armor_character.b3d", textures = { "character_27.png",
			"3d_armor_trans.png",
			minetest.registered_items[ "default:pick_bronze" ].inventory_image
		} }

minetest.register_entity( "holoblock:hologram", {
	hp_max = 1,
        physical = false,

	collisionbox = { 0, 0, 0, 0, 0, 0 },
	drawtype = "front",
	visual = "mesh",
	visual_size = { x = 1, y = 1 },
	textures = { "air" },

	on_activate = function( self, staticdata, dtime )
		if staticdata ~= "" then
			--print( "Reactivating hologram 0x" .. staticdata )
			self.key = tonumber( staticdata, 16 )
			self.pos = minetest.get_position_from_hash( self.key )

			if minetest.get_node( self.pos ).name ~= "holoblock:holoblock" then
				-- sanity check for hologram with missing actuator
				self.object:remove( )
				return
			end

			self:reset( holo_defs[ minetest.get_meta( self.pos ):get_int( "index" ) ] )
			holo_objs[ self.key ] = self.object

		elseif dtime > 0 then
			-- sanity check for hologram with invalid key
			self.object:remove( )
			return
		end

		self.is_init = true
	end,
	reset = function( self, def )
		if not def then
			self.object:remove( )
			return
		end

		self.object:set_properties( { mesh = def.mesh, textures = def.textures, visual_size = def.size } )
		self.object:setpos( { x = self.pos.x + def.x, y = self.pos.y + def.y - 1, z = self.pos.z + def.z } )
		self.object:setyaw( math.rad( def.yaw ) )
	end,
	get_staticdata = function( self )
		if self.is_init == false then
			local staticdata = string.format( "%x", self.key )

			--print( "Deactivating hologram 0x" .. staticdata )
			holo_objs[ self.key ] = nil

			return staticdata
		end
		self.is_init = false
	end,
} )

minetest.register_node( "holoblock:holoblock", {
        description = "Hologram Actuator",
        drawtype = "glasslike",
        tiles = { "holoblock.png" },
        is_ground_content = false,
        light_source = LIGHT_MAX,
        groups = { cracky=1, level=3 },
        sounds = default.node_sound_stone_defaults( ),

	can_dig = function( pos, player )
		return  default.is_owner( pos, player:get_player_name( ) )
	end,
	on_rightclick = function( pos, node, player )
		if  default.is_owner( pos, player:get_player_name( ) ) then
			local obj = holo_objs[ minetest.hash_node_position( pos ) ]
			local def_index = minetest.get_meta( pos ):get_int( "index" )

			def_index = def_index < #holo_defs and def_index + 1 or 1
			minetest.get_meta( pos ):set_int( "index", def_index )
			obj:get_luaentity( ):reset( holo_defs[ def_index ] )
		end
	end,
	on_timer = function( pos, elapsed )
		local obj = holo_objs[ minetest.hash_node_position( pos ) ]
		local def = holo_defs[ minetest.get_meta( pos ):get_int( "index" ) ]

		if not obj then return end
                if #( minetest.get_objects_inside_radius( vector.add( pos, { x = 0, y = 0.5, z = 0 } ), 1.0 ) ) > 0 then
			obj:set_animation( { x = def.anim_start, y = def.anim_end }, def.anim_speed, 0 )
		else
			obj:set_animation( { x = 0, y = 0 }, 0, 0 )
		end
		return true
	end,
	after_place_node = function( pos, placer, itemstack )
		local def = holo_defs[ 1 ]

		local obj = minetest.add_entity( { x = pos.x + def.x, y = pos.y + def.y - 1, z = pos.z + def.z }, "holoblock:hologram" )
		local key = minetest.hash_node_position( pos )
		local pname = placer:get_player_name( )
		local meta = minetest.get_meta( pos )
		local self = obj:get_luaentity( )

		meta:set_string( "infotext", "Hologram Actuator (Owned by " .. pname .. ")" )
		meta:set_int( "index", 1 )
		meta:set_string( "owner", pname )
		minetest.get_node_timer( pos ):start( 1 )

		self.key = key
		self.pos = pos
		self:reset( def )

		holo_objs[ key ] = obj
        end,
	after_dig_node = function( pos, node, metadata, digger )
		local key = minetest.hash_node_position( pos )
		local obj = holo_objs[ key ]

		if obj then	-- sanity check since corresponding object might not be reactivated
			obj:remove( )
		end
	end,
} )
